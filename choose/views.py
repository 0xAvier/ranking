from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.forms.models import model_to_dict

from .models import Choice, User


def _get_user(form_input):
    u = User.objects.get(rank = int(form_input.split(':')[0][1:]))
    return {'name': u.name, 'rank': u.rank}


def index(request):
    request.session.setdefault('user', None)
    request.session.setdefault('gift', None)

    gifts = Choice.objects.order_by('number') 
    users = User.objects.order_by('rank')

    context = {'gifts': gifts, 'users': users, 'user': request.session['user'], 
            'gift': request.session['gift']}

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if 'back' in request.POST.keys():
            request.session['user'] = None
            context['user'] = None 
            return render(request, 'choose/index.html', context)
        if 'choice' in request.POST.keys():
            request.session['gift'] = request.POST['choice']
            context['choice'] = request.session['gift'] 
            return render(request, 'choose/index.html', context)
        elif 'user' in request.POST.keys():
            request.session['user'] = _get_user(request.POST['user'])
            context['user'] = request.session['user']
            return render(request, 'choose/index.html', context)
        else:
            return render(request, 'choose/index.html', context)

    return render(request, 'choose/index.html', context)

