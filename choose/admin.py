from django.contrib import admin

# Register your models here.
from .models import User, Choice 

admin.site.register(User)
admin.site.register(Choice)

