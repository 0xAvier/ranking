from django.db import models

# Number
# Name
class Choice(models.Model):
    name = models.CharField(max_length=200)
    number = models.IntegerField()

# Create your models here.
# User
# Name, Rank, Mail, Choices[3] 
class User(models.Model):
    name = models.CharField(max_length=200)
    rank = models.IntegerField()
    choice = models.ForeignKey('Choice', on_delete=models.PROTECT, null=True)

